import kotlin.random.Random

class CardsDeck {
    lateinit var cardDeck: MutableList<Card>

    init {
        generateCardDeck()
    }

    fun generateCardDeck() {
        this.cardDeck = mutableListOf()

        generateCards("Spade")
        generateCards("Heart")
        generateCards("Club")
        generateCards("Diamond")

        this.cardDeck.shuffle()
    }

    private fun generateCards(newColor: String) {
        cardDeck.add(Card(newColor, 2))
        cardDeck.add(Card(newColor, 3))
        cardDeck.add(Card(newColor, 4))
        cardDeck.add(Card(newColor, 5))
        cardDeck.add(Card(newColor, 6))
        cardDeck.add(Card(newColor, 7))
        cardDeck.add(Card(newColor, 8))
        cardDeck.add(Card(newColor, 9))
        cardDeck.add(Card(newColor, 10))
        cardDeck.add(Card(newColor, 10, "Fool"))
        cardDeck.add(Card(newColor, 10, "King"))
        cardDeck.add(Card(newColor, 10, "Queen"))
        cardDeck.add(Card(newColor, 11, "As"))
    }

    fun pop(): Card {
        return this.cardDeck.removeAt(this.cardDeck.size - 1)
    }
}