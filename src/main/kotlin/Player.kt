abstract class Player(name: String, isComputer: Boolean, deck: CardsDeck) {
    val name = name
    val isComputer: Boolean = isComputer
    var activeDeck: CardsDeck = deck
    var currentPoints: Int = 0

    abstract fun popCard(): Card
}

class HumanPlayer(name: String, deck: CardsDeck) : Player(name, false, deck) {
    override fun popCard(): Card {
        val card = activeDeck.pop()
        currentPoints += card.cardValue

        return card
    }
}

class ComputerPlayer(deck: CardsDeck) : Player("AI Card Dealer", true, deck) {
    override fun popCard(): Card {
        if (currentPoints <= 16) {
            val card = activeDeck.pop()
            return if ((card.picture == "As") && ((this.currentPoints + 11) > 21)) {
                this.currentPoints += 1
                Card(card.color, 1, card.picture)
            } else {
                this.currentPoints += card.cardValue
                card
            }
        }

        return Card()
    }
}